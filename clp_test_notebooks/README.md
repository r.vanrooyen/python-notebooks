# Python Notebooks
Example IPython notebooks to inspection behaviour of possible signal processing verification needed in SCP integration.

## Notes for CNIC to CNIC notebook
```
Connect to Topic-SKA wifi

You can inspect which servers you have running on notebooks by going to    
http://k8s.clp.skao.int/binderhub/jupyterhub/hub/home

In chrome (browser)
http://k8s.clp.skao.int/binderhub/

binderhub:
* change GitHub to GitLab.com,
* fill in URL: ska-telescope/ska-low-csp and
* fill in the branch name if not using the main branch -- Git ref: top-489-omc-art-demo
* click launch and 
* wait for it to lauch;
* navigate to notebooks/ and
* select the notebook to run
* run notebook cells

when rx/tx servers assigned, open them on taranta low-cbf

taranta controllers:
http://k8s.clp.skao.int/ska-low-csp-integration/taranta/devices

select the associated processors
(numbers assigned: 0.0.3, 0.0.5)
and view Attributes

in ubuntu shell
ssh ubuntu@clp-k8s-fpga-worker-1
cd mnt/ssd
ls -alh
-rw-r--r-- 1 root   root   195M May 22 14:29 LFAATestData.pcap (input file transmitted)
-rw-r--r-- 1 ubuntu ubuntu 195M May 24 12:22 rx.pcap (output file received)
number of packets transmitted: 24480
```

-fin-
